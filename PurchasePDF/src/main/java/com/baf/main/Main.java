/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.baf.main;

import com.baf.dao.CustomerDao;
import com.baf.function.CreatedPdf;
import com.baf.function.GeneralConstanta;
import com.baf.model.CustomerModel;
import com.baf.model.DateModel;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sourceforge.jtds.jdbc.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author 0414831216
 */
public class Main {
    public static void main(String[] args) throws JRException, IOException {
        // Compile jrxml file.
       
        ApplicationContext context = 
    		new ClassPathXmlApplicationContext("Spring-Modul.xml");
        CustomerDao customerDao = (CustomerDao) context.getBean("customerDAO");

        Properties props = GeneralConstanta.props();
        String rootDirectory = props.getProperty("directoryparent");
        
        
        List<CustomerModel> customerAs = customerDao.customer(rootDirectory);

    }
} 
