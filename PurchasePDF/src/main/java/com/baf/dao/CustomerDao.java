/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.baf.dao;

import com.baf.model.CustomerModel;
import java.util.List;

/**
 *
 * @author 0414831216
 */
public interface CustomerDao {
    public List<CustomerModel> customer(String rootDirectory);
}
