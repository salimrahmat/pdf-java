/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.baf.implement;

import com.baf.dao.CustomerDao;
import com.baf.function.CreatedPdf;
import com.baf.function.GeneralFunction;
import com.baf.function.GeneralConstanta;
import com.baf.model.CustomerModel;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataSource;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Component;

/**
 *
 * @author 0414831216
 */
@Component
public class CustomerImplements extends JdbcDaoSupport implements CustomerDao {
    private DataSource dataSource;
    private Connection conn = null;
    
    @Override
    public List<CustomerModel> customer(String rootDirectory) {
        CreatedPdf pdf = new CreatedPdf();
        
        try {
            conn = getJdbcTemplate().getDataSource().getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(CustomerImplements.class.getName()).log(Level.SEVERE, null, ex);
        }
        String sql = " SELECT TOP 10 " +
                     " ISNULL (RO.OFFICE_CODE,'-') AS OFFICE_CODE," +
                     " ISNULL (SB.SUPPL_ID,'-') AS SUPPL_ID," +
                     " ISNULL (APP.APP_NO,'-') AS APP_NO," +
                     " ISNULL (CONVERT(VARCHAR(10),CP.BIRTH_DT,103),'-') AS BIRTH_DT," +
                     " ISNULL (CONVERT(VARCHAR(10),POH.PURCHASE_ORDER_DT,103),'-') AS PO_DATE," +
                     " PURCHASE_ORDER_NO AS PO_NO," +
                     " convert(varchar(8),cast(getdate() + 1 as date),112) AS DATE_NOW" +
                     " FROM APP " +
                     " INNER JOIN CUST ON APP.CUST_ID=CUST.CUST_ID " +
                     " LEFT JOIN AGRMNT_ASSET ON APP.APP_ID=AGRMNT_ASSET.APP_ID " +
                     " LEFT JOIN AGRMNT_ASSET_SUPPL ON AGRMNT_ASSET.AGRMNT_ASSET_ID = AGRMNT_ASSET_SUPPL.AGRMNT_ASSET_ID " +
                     " LEFT JOIN SUPPL_BRANCH SB ON AGRMNT_ASSET_SUPPL.SUPPL_BRANCH_ID = SB.SUPPL_BRANCH_ID " +
                     " INNER JOIN REF_OFFICE RO ON APP.REF_OFFICE_ID = RO.REF_OFFICE_ID " +
                     " INNER JOIN CUST_PERSONAL CP ON CUST.CUST_ID=CP.CUST_ID " +
                     " LEFT JOIN AGRMNT ON APP.APP_ID = AGRMNT.APP_ID " +
                     " INNER JOIN PURCHASE_ORDER_H POH ON AGRMNT.AGRMNT_ID = POH.AGRMNT_ID " +
                     " WHERE APP.APP_STEP IN ('DLO','POP','DSG') " +
                     " AND POH.PURCHASE_ORDER_DT BETWEEN CAST('28-JUN-2019 00:00:00' AS DATETIME) AND CAST('28-JUN-2019 23:59:00' AS DATETIME)";
		 
		List<CustomerModel> customers = new ArrayList<CustomerModel>();
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
		CustomerModel cm = new CustomerModel();
                for (Map row : rows) {
			cm.setApp_No((String) row.get("APP_NO"));
                        cm.setBirth_Dt((String)row.get("BIRTH_DT"));
                        cm.setDate_Now((String)row.get("DATE_NOW"));
                        cm.setOffice_Code((String) row.get("OFFICE_CODE") );
                        cm.setPo_Date((String)row.get("PO_DATE"));
                        cm.setSuppl_Id(String.valueOf(row.get("SUPPL_ID")));
                        cm.setPo_No((String)row.get("PO_NO"));
                        
                        try {
                            pdf.GeneratePdf(conn, cm,rootDirectory);
                        } catch (JRException ex) {
                            Logger.getLogger(CustomerImplements.class.getName()).log(Level.SEVERE, null, ex);
                        }
			customers.add(cm);
		}
		return customers;
    }
    
}
