/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.baf.model;

import java.io.Serializable;

/**
 *
 * @author 0414831216
 */
public class CustomerModel implements Serializable{
    private String office_Code;	
    private String suppl_Id;	
    private String app_No;	
    private String birth_Dt;	
    private String po_Date;	
    private String po_No;	
    private String date_Now;

    /**
     * @return the office_Code
     */
    public String getOffice_Code() {
        return office_Code;
    }

    /**
     * @param office_Code the office_Code to set
     */
    public void setOffice_Code(String office_Code) {
        this.office_Code = office_Code;
    }

    /**
     * @return the suppl_Id
     */
    public String getSuppl_Id() {
        return suppl_Id;
    }

    /**
     * @param suppl_Id the suppl_Id to set
     */
    public void setSuppl_Id(String suppl_Id) {
        this.suppl_Id = suppl_Id;
    }

    /**
     * @return the app_No
     */
    public String getApp_No() {
        return app_No;
    }

    /**
     * @param app_No the app_No to set
     */
    public void setApp_No(String app_No) {
        this.app_No = app_No;
    }

    /**
     * @return the birth_Dt
     */
    public String getBirth_Dt() {
        return birth_Dt;
    }

    /**
     * @param birth_Dt the birth_Dt to set
     */
    public void setBirth_Dt(String birth_Dt) {
        this.birth_Dt = birth_Dt;
    }

    /**
     * @return the po_Date
     */
    public String getPo_Date() {
        return po_Date;
    }

    /**
     * @param po_Date the po_Date to set
     */
    public void setPo_Date(String po_Date) {
        this.po_Date = po_Date;
    }

    /**
     * @return the po_No
     */
    public String getPo_No() {
        return po_No;
    }

    /**
     * @param po_No the po_No to set
     */
    public void setPo_No(String po_No) {
        this.po_No = po_No;
    }

    /**
     * @return the date_Now
     */
    public String getDate_Now() {
        return date_Now;
    }

    /**
     * @param date_Now the date_Now to set
     */
    public void setDate_Now(String date_Now) {
        this.date_Now = date_Now;
    }
    
    
   @Override
	public String toString() {
		return "CustomerModel [office_Code=" + office_Code + ", suppl_Id=" + suppl_Id + ", app_No=" + app_No
				+ ", birth_Dt=" + birth_Dt + ", po_Date=" + po_Date + ", po_No=" + po_No + ", date_Now=" + date_Now + "]";
	}

}	
