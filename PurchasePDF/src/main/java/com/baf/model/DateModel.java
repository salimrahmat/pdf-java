/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.baf.model;

/**
 *
 * @author 0414831216
 */
public class DateModel {
    private Integer jam;
    private String rangeJam;
    
    /**
     * @return the jam
     */
    public Integer getJam() {
        return jam;
    }

    /**
     * @param jam the jam to set
     */
    public void setJam(Integer jam) {
        this.jam = jam;
    }

    /**
     * @return the rangeJam
     */
    public String getRangeJam() {
        return rangeJam;
    }

    /**
     * @param rangeJam the rangeJam to set
     */
    public void setRangeJam(String rangeJam) {
        this.rangeJam = rangeJam;
    }
    
    
    
    
}
