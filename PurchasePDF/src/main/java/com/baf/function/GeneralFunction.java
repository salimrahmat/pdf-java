/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.baf.function;

import com.baf.model.DateModel;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author 0414831216
 */
public class GeneralFunction {
    
   public static String createDirectory(String diretoryRoot,String directoryName) {
      String nameDirectory;  
      String nameDirPdf;      
      nameDirPdf = getTimeFolder();
      nameDirectory = diretoryRoot+directoryName+"/"+nameDirPdf;  
      File outDir = new File(nameDirectory);
        boolean exists = outDir.exists();
        if(!exists){
           outDir.mkdirs();
        }
       return nameDirectory;
   }
   
   
   public static String getTimeFolder(){
        String result = null;
        Date dt = new Date(); 
        
        List<DateModel> dateModels = new ArrayList<DateModel>();
        int jam=8;
        for(int i=0;i<=16;i++){
            DateModel dm = new DateModel();
            dm.setJam(jam);
            dm.setRangeJam(String.valueOf(jam)+"-"+String.valueOf(jam + 2));
            jam ++;
            
            dateModels.add(dm);
        }
        
        for (DateModel dateModel: dateModels) {
             if(dateModel.getJam()==dt.getHours()){
                 result = dateModel.getRangeJam();
                 break;
             } 
        }   
       return result;
   }
   
}
