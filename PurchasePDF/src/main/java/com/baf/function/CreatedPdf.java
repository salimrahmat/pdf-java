/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.baf.function;

import com.baf.model.CustomerModel;
import com.itextpdf.text.pdf.PdfWriter;
import static groovy.inspect.Inspector.print;
import java.io.File;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import org.exolab.castor.mapping.xml.Param;

/**
 *
 * @author 0414831216
 */
public class CreatedPdf {
    private String nameRootDirectory;
    
    
    public void GeneratePdf(Connection conn,CustomerModel customerModel, String rootDirectory) throws JRException{
        JasperReport jasperReport = JasperCompileManager
               .compileReport("E:/Salimrahmat/RFC/PO_PDF/PurchasePDF/src/main/java/com/baf/report/PoPdf.jrxml");
  
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("P_OfficeCode", customerModel.getOffice_Code());
        parameters.put("P_supplier", customerModel.getSuppl_Id());
        parameters.put("P_aplikasiid", customerModel.getApp_No());


        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);

        
        nameRootDirectory = GeneralFunction.createDirectory(rootDirectory, customerModel.getDate_Now());
        
//        File outDir = new File("E:/testing/"+customerModel.getDate_Now());
//        boolean exists = outDir.exists();
//        if(!exists){
//           outDir.mkdirs();
//        }
        
         // PDF Exportor.
        JRPdfExporter exporter = new JRPdfExporter();
 
        ExporterInput exporterInput = new SimpleExporterInput(jasperPrint);
        // ExporterInput
        exporter.setExporterInput(exporterInput);
 
        // ExporterOutput
        OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
                nameRootDirectory+"/"+customerModel.getPo_No()+".pdf");
        // Output
        exporter.setExporterOutput(exporterOutput);
        
        SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
        configuration.setEncrypted(true);
//        configuration.set128BitKey(true);
        configuration.setUserPassword(customerModel.getBirth_Dt());
//        configuration.setOwnerPassword("reports");
        configuration.setPermissions(PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING);
        exporter.setConfiguration(configuration);
        exporter.exportReport();
 
        System.out.print("Done!");
    }
    
    
}
